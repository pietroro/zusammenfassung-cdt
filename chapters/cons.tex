% !TeX root = ../zsfg-cdt.tex

\section{CONS}
A CONS is a complete orthonormal system. The sequence of signals $..., \bm{\phi}_{-1}, \bm{\phi}_{0},\bm{\phi}_{1}, ...$ is said to form a CONS for some subspace U if all three following conditions hold:
\begin{enumerate}
	\item Each  element is in the subspace U
	\item The sequence satisfies orthonormality ($\innerp{\phi_l}{\phi_{l'}} = I\{l = l'\}$)
	\item For every $\bm u$ we have $$||\bm u||^2_2 = \sum_{l = -\infty}^{\infty}|\innerp{\bm u}{\bm{\phi}_l}|^2$$
\end{enumerate}
\textbf{A useful Property:}\\
If $\{\psi_l\}$ is a CONS for the subspace $\nu$ then $\{\check{\psi}_l\}$ is a CONS for the subspace $\check{\nu}$\\\\
This leads then to some other properties that can be seen on page 149 in the book!
\subsection{Fourier}
The functions
\begin{align}
\bm{\phi}_l: t\mapsto \frac{1}{\sqrt{2T}}e^{i\pi lt/T}I\{|t|\leq T\}\\
\intertext{form a CONS of the subspace}
\left\{ \bm u \in \mathcal{L}_2 : u(t) = 0 \text{ whenever } \vert t\vert >
T\right \}.
\intertext{In this case}
\innerp{\bm u}{\bm{\phi}_l} = \frac{1}{\sqrt{2T}}\int_{-T}^{T}u(t)e^{-i\pi lt/T}dt
\end{align}
which is the $l$-th fourrier series coefficient.

Replacing $t$ by $f$ and $T$ by $W$, the functions
\begin{align}
  &f\mapsto \frac{1}{\sqrt{W}}e^{i\pi lf/W}I\{|f|\leq
  W\}\\
\intertext{form a CONS of the subspace}
\label{eq:consv} \mathcal{V} =& \left\{ \bm g \in \mathcal{L}_2 : g(f) = 0 \text{ whenever } \vert f\vert >
W\right \}.
\end{align}
\subsection{The Sampling Theorem}
We can get a CONS for bandlimited signals of $W$ Hz by just inverse Fourier
transform of the CONS $\mathcal{V}$ in \eqref{eq:consv} above. This yields us the \textbf{CONS for bandlimited signals:}
\begin{align}
&\bm{\phi_l}: t\mapsto \sqrt{2W}sinc(2Wt+l)\\
\intertext{with}
&\eqbox{\innerp{\bm x}{t\mapsto \sqrt{2W}sinc(2Wt+l)} =
\frac{1}{\sqrt{2W}}x\left(-\frac{l}{2W}\right)}
\end{align}
\textbf{Definition of the Sampling Theorem:}\\
$\bm x$ bandlimited to $W$ Hz with $0 < T \leq \frac{1}{2W}$. Then the signal can be reconstructed just by its samples trough
$$\underset{L \rightarrow \infty}{\text{lim}}\intf|x(t) - \sum_{l = -L}^{L}x(-lT)sinc(\frac{t}{T}+l)|^2dt = 0$$
Furthermore the energy of the signal can also be reconstructed
$$\intf|x(t)|^2dt = T\sum_{l = -\infty}^{\infty}|x(lT)|^2$$
And if $\bm y$ is also bandlimited to $W$ Hz then
$$\innerpf{x}{y} = T\sum_{l = -\infty}^{\infty}x(lT)y^*(lT)$$
\textbf{Pointwise calculation:}
The error goes to zero as $L$ goes to infinity
$$x(t) = \underset{L\rightarrow \infty}{\text{lim}}\sum_{l = -L}^{L}x(-lT)sinc(\frac{t}{T}+l)$$
\textbf{Samples of a convolution:} where $\bm x$ and $\bm y$ are bandlimited to
$W$ Hz
$$(\bm{x\star y})(\frac{l}{2W})  = \frac{1}{2W}\sum_{\nu = -\infty}^{\infty}x(\frac{\nu}{2W})y(\frac{l-\nu}{2W})$$\\
\subsection{Bandlimited-Signals and Square Summable Sequences}
Let $T=1/(2W), W>0$ and let $u,v$ be energy-limited, bandlimited to $W$ Hz signals.
\begin{itemize}
    \item $\mathrm{T} \sum_{\ell=-\infty}^{\infty}|u(\ell \mathrm{T})|^{2}=\|\mathbf{u}\|_{2}^{2}$
    \item $\mathrm{T} \sum_{\ell=-\infty}^{\infty} u(\ell \mathrm{T}) v^{*}(\ell\mathrm{T})=\langle\mathbf{u}, \mathbf{v}\rangle$
    \item Let $\alpha_\ell$ be a bi-infinite square-summable sequence then $u(\ell \mathrm{T})=\alpha_{\ell}, \quad \ell \in \mathbb{Z}$
    \item The mapping that maps every energy-limited signal that is bandlimited to $W Hz$ to the square-summable sequence consisting of its samples is linear.
\end{itemize}

% \section{Gram-Schmidt}
% \textbf{Vorgehen:}
% \begin{itemize}
% 	\item \textbf{Schritt 1:} Bilde normalisierte Basisfunktion $\Phi_1(t)$
% 		$$\Phi_1(t) = \frac{s_1(t)}{(\int_{0}^{T} s_1(t)^2 dt)^{1/2}}$$

% 	\item \textbf{Schritt 2:} Suche Basisfunktion $\Phi_2(t)$ von $s_2(t)$
% 		\begin{align}
% 			s_{21} &= \langle s_2(t),\Phi_1(t) \rangle =
% 			\int_{0}^{T} s_2(t) \cdot \Phi_1(t) dt\\
% 			g_2 &= (s_2(t) - s_{21}(t) \cdot \Phi_1(t)\\
% 			\Phi_2(t) &= \frac{g_2(t)}{(\int_{0}^{T} g_2(t)^2 dt)^{1/2}}
% 		\end{align}

% 	\item \textbf{Schritt 3:}  Suche Basisfunktion $\Phi_3(t)$ von $s_3(t)$
% 		\begin{align}
% s_{31} &= \langle s_{i3}(t),\Phi_1(t) \rangle = \int_{0}^{T} s_3(t) \cdot \Phi_1(t) dt\\
% s_{32} &= \langle s_3(t),\Phi_2(t) \rangle = \int_{0}^{T} s_3(t) \cdot \Phi_2(t) dt\\
% g_3 &= (s_3(t) - s_{31}(t) \cdot \Phi_1(t) - s_{33}(t) \cdot \Phi_2(t)\\
% \Phi_3(t) &= \frac{g_3(t)}{(\int_{0}^{T} g_3(t)^2 dt)^{1/2}}
% 		\end{align}
% \end{itemize}
% Wenn $g_2(t) = 0$, dann ist $s_2(t)$ linear unabhängig von $\Phi_1(t)$.
