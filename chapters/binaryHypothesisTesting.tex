% !TeX root = ../zsfg-cdt.tex

\section{Binary Hypothesis Testing}
\subsection{Definitions}
The \textbf{prior} is the distribution of H, reflecting our knowledge about H
without any observation (the pair $(\pi_0, \pi_1)$.

The prior is \textbf{uniform} if $\pi_0=\pi_1 = 1$\\
The \textbf{observation} is a random vector denoted as Y. If its dimension is d=1 then we have just one observation
$$\bm Y = (Y^{(1)},...,Y^{(d)})^\text{T}$$
We define two densities of interest
$$f_{\bm Y|H = 0}(\cdot)\qquad f_{\bm Y|H=1}(\cdot)$$
The  \textbf{Guessing Rule} is a rule for decoding H by looking at Y (the input of $\phi$ is $\bm y_{obs}$)
$$\phi_{guessing}:\mathbb{R}^d\rightarrow\{0,1\}$$
The \textbf{Probability of Error} associated with the guessing rule is
$$\text{Pr(error)} = \text{Pr}[\phi_{Guess}(\bm Y)\neq H]$$
The \textbf{optimal probability of error} is denoted by $p^*(error)$
\subsection{Guessing without Y}
The optimal guessing rule is
$$\phi_{Guess}^* = \begin{cases}0&\text{if }\pi_0\geq \pi_1\\
1& otherwise\end{cases}$$
This leads to an optimal error of
$$p^*(error) = min\{\pi_0,\pi_1\}$$
\subsection{Joint law of \texorpdfstring{$\bm Y$ and $H$}{Y and H}}
$$\eqbox{f_{\bm Y}(\bm y) = \pi_0f_{\bm Y|H=0}(\bm y)+\pi_1f_{\bm Y|H=1}(\bm y)}$$
$$Pr[H=0|\bm Y = \bm y_{obs}] = \begin{cases}\frac{\pi_0f_{\bm Y |H = 0}(\bm y_{obs})}{f_{\bm Y}(\bm y)}& \text{if }f_{\bm Y}(\bm y_{obs}) > 0\\
\frac{1}{2}& \text{otherwise}\end{cases}$$
$$Pr[H=1|\bm Y = \bm y_{obs}] = \begin{cases}\frac{\pi_1f_{\bm Y |H = 1}(\bm y_{obs})}{f_{\bm Y}(\bm y)}& \text{if }f_{\bm Y}(\bm y_{obs}) > 0\\
\frac{1}{2}& \text{otherwise}\end{cases}$$
With the two functions above one can see that the density of $\bm Y$ alone is not that important because we know that the it doesnt matter for knowing which probability is bigger. It just matters to know the exact probability which is not that important.

As a reminder: the \textbf{conditional probability} is defined as
$$P_{X|Y}(x|y) = \frac{P_{X,Y}(x,y)}{P_Y(y)}$$
\subsection{Guessing after Observing Y}
Once we know $\bm Y$ we dont want to just guess a priory. We want to use this information. In this case it is obvious to guess based on
$$\phi_{Guess}^*(\bm y_{obs}) =$$ $$ \begin{cases}0& \text{if } Pr[H=0|\bm Y = \bm y_{obs}]\geq Pr[H=1|\bm Y= \bm y_{obs}]\\
1&\text{otherwise}\end{cases}$$
$$ \eqbox{ = \begin{cases}0& \text{if } \pi_0f_{\bm Y|H = 0}(\bm y_{obs})\geq \pi_1f_{\bm Y|H = 1}(\bm y_{obs})\\
1&\text{otherwise}\end{cases}}$$
With the conditional  error on the observation being
$$p^*(error|\bm Y = \bm y_{obs}) = $$$$min\{Pr[H=0|\bm Y=\bm y_{obs}],Pr[H=1|\bm Y=\bm y_{obs}]\}$$
The whole error is then
$$p^*(error) = \int_{\mathbb{R}^d}\text{min}\{\pi_0f_{\bm Y|H=0}(\bm y),\pi_1f_{\bm Y|H=1}(\bm y)\}d\bm y$$
Useful for calculations:
$$\eqbox{p(error|H = 0) = \int_{\bm y \notin \mathcal D} f_{\bm Y|H=0}(\bm y) d\bm y}$$
$$\eqbox{p(error|H=1) = \int_{\bm y \in \mathcal D} f_{\bm Y|H=1}(\bm y)d\bm y}$$
with $\mathcal D = \{\bm y\in \mathbb{R}^d:\phi_{Guess}(\bm y)=0\}$\\\\
Usually one calculates the bound first. If I have then the bound where $H = 0$
is guessed then for the error I integrate the conditional probability on $H=1$ over this bound and vice versa. \textbf{The unconditional error can then also be calculated by first multiplying with the prior each and then adding the two.}
\subsection{Likelihood functions}
There are two likelihood-functions of interest, first the normal LR function which is defined as
$$LR(\bm y) = \frac{f_{\bm Y|H=0}(\bm y)}{f_{\bm Y|H=1}(\bm y)}$$
and the log likelihood function which is pariculary suited for gaussian densities
$$LLR(\bm y) = ln(\frac{f_{\bm Y|H=0}(\bm y)}{f_{\bm Y|H=1}(\bm y)})$$
\subsection{MAP decision rule}
Something that was left out before was what to do if the probabilities are equal. The MAP-rule covers this
\begin{center}
	\includegraphics[width=\linewidth]{images/MAP}
\end{center}
One can use the \textbf{Likelihood-ratio} function also to describe the MAP-Decision:
\begin{center}
	\includegraphics[width=\linewidth]{images/MAPLR}
\end{center}
Or the \textbf{log likelihood-ratio} function:
\begin{center}
	\includegraphics[width=\linewidth]{images/MAPLLR}
\end{center}
\subsection{ML decision rule}
This rule is exactly the same than MAP decision but we assume that we don't know the priors, or at least don't use them. This is equal to saying that the priors are equal, i.e. $\pi_0 = \pi_1 = \frac{1}{2}$. This is usually a suboptimal guessing rule but we have to use it if the prior is not known.
\subsection{The Bhattacharyya Bound}
This bound is an upper bound for the probability error
$$p^*(error) \leq \frac{1}{2}\int_{\mathbb{R}^d}\sqrt{f_{\bm Y|H=0}(\bm y)f_{\bm Y|H=1}(\bm y)} d\bm y$$
The derivation is found on P.408. Furthermore if the priors are known the $\frac{1}{2}$ can be changed by $\sqrt{\pi_0\pi_1}$ for a more accurate upper bound.
\subsection{(Nontelepathic) Processing}
What we mean by this are two things: first applying some function g onto our guessing variable $y_{obs}$ will not yield a better guessing rule.\\
The second thing is that by randomly creating another guessing variable and
then guess based on the two variables will also not yield a better guessing
rule. This is obvious as adding a variable not related to $\bm y$ to the density function we can just split it away from the conditional density function and then it cancels itself out (P.412).
\subsection{Implications of Optimality}
It is well explained on P.424, but the idea is that once we have calculated the LR function of some Hypothesis then we can calculate other LR functions of other Hypothesises with other densities IF the mapping from one variable has a one to one relationship, for example
$$ T = Y-\frac{1}{2}$$
Because of the section above this guessing rule then is not worse than the optimal guessing rule!
\subsection{Multi Dimensional binary gaussian Hypothesis Testing}
\subsubsection{Setup}
The variable can take the values 0 and 1 with probability $\pi_0,\pi_1$.
Conditional on $H = 0$ the components of $\bm Y$ are independent gaussians with
$Y^{(j)} \backsim \mathcal N(s_0^{(j)},\sigma^2)$ and conditional on $H = 1$ the components are also independent gaussians with $Y^{(j)} \backsim \mathcal N(s_1^{(j)},\sigma^2)$. The $\bm s$ vectors are of the form
$$\bm s_0 = (s_0^{(1)},...,s_0^{(J)})\qquad \bm s_1 = (s_1^{(1)},...,s_1^{(J)})$$
Where they are different in at least one coordinate.
\textbf{The setup can therefore be written as}
$$H = 0: Y^{(j)} = s_0^{(j)}+ Z^{(j)}$$
$$H = 1: Y^{(j)} = s_1^{(j)}+ Z^{(j)}$$
with the components of $\bm Z$ being IID $\mathcal N(0,\sigma^2)$
\subsubsection{Decision Rule}
One can then compute the $LR$ and $LLR$ function (P.426). When calculating keep in mind that all the gaussians are independent, so one can just make a product from 1 to J for the conditional probability for $\bm y|H = 0$ and the same for $\bm y|H=1$ and then divide the two.\\\\
At the end the solution is

\begin{equation}
  LLR(\bm y) =\frac{||\bm s_0-\bm s_1||}{\sigma^2}(\innerp{\bm
	       y}{\bm{\phi}}_E-\frac{1}{2}(\innerp{\bm
	     s_0}{\bm{\phi}}_E+\innerp{\bm s_1}{\bm{\phi}}_E))
\end{equation}
	   With $\bm{\phi} = \frac{\bm s_0-\bm s_1}{||\bm s_0-\bm s_1||}$ and a new
	   definition for the inner product with subscript $E$
	   $$\innerpf{u}{v}_E = \sum_{j=1}^{J}u^{(j)}v^{(j)}\quad ||\bm u|| = \sqrt{\innerpf{u}{u}_E}$$
	   where $\bm u$ is real.\\\\
	   $H = 0$ if $LLR(\bm y) \geq ln\frac{\pi_1}{\pi_0} \rightarrow$ Guess
	   $H = 0$ if
	   $$\eqbox{\innerp{\bm y}{\bm{\phi}}_E \geq \frac{\innerp{\bm s_0}{\bm{\phi}}_E+\innerp{\bm s_1}{\bm{\phi}}_E}{2}+\frac{\sigma^2}{||\bm s_0-\bm s_1||}ln(\frac{\pi_1}{\pi_0})}$$
	   This rule is depicted in this image:
	   \begin{center}
	     \includegraphics[width=\linewidth]{images/MDBG}
	   \end{center}
	   If we have an uniform prior we can calculate the $LLR$ without knowing $\sigma$:\\
	   Guess $H=0$ if
	   $$\innerp{\bm y}{\bm{\phi}}_E \geq \frac{\innerp{\bm s_0}{\bm{\phi}}_E+\innerp{\bm s_1}{\bm{\phi}}_E}{2}$$
	   \subsubsection{Error Analysis}
	   We need to compute
	   $$p^*(error) = \pi_0p(error|H=0)+\pi_1p(error|H=1)$$
	   with \\
	   $p^*(error|H=0) =$ $$ Pr(\pi_0f_{\bm Y|H=0}(\bm Y) < \pi_1f_{\bm Y|H=1}(\bm Y)|H=0)$$
	   with the lemma on p.428 one can then calculate this (???) and becomes the result on P.430.\\
	   \textbf{If the priors are uniform then}
	   $$p^*(error) = Q(\frac{||\bm s_0-\bm s_1||}{2\sigma})$$
	   The bhattacharyya Bound is computed on the next side of the book.
	   \subsection{Guessing in the Presence of a Random Parameter}
	   \subsubsection{Random Parameter not Observed}
	   Usually we are given the density function as $f_{\bm Y,\Theta|H =
	   0}$ (same for $H=1$). Then one can compute
	   $$f_{\bm Y|H = 0}(\bm y_{obs}) = \int_{\theta}f_{\bm Y,\Theta|H=0}(\bm y_{obs},\theta)d\theta$$
	   $$ = \int_{\theta}f_{\bm Y|\Theta = \theta, H = 0}(\bm y_{obs})f_{\Theta}(\theta)d\theta$$
	   Here we assume that $Y$ and $\Theta$ are independent. We thus become
	   $$\eqbox{LR(\bm y_{obs}) = \frac{\int_{\theta}f_{\bm Y|\Theta = \theta, H = 0}(\bm y_{obs})f_{\Theta}(\theta)d\theta}{\int_{\theta}f_{\bm Y|\Theta = \theta, H = 1}(\bm y_{obs})f_{\Theta}(\theta)d\theta}}$$
	   \subsubsection{Random Parameter Observed}
	   In this case we just base the likelihood-ratio function on both parameter:
	   $$LR(\bm y_{obs},\theta_{obs}) = \frac{f_{\bm Y,\Theta|H=0}(\bm y_{obs},\theta_{obs})}{f_{\bm Y,\Theta|H=1}(\bm y_{obs},\theta_{obs})}$$
	   And because we can split up these conditional probabilities ($Y$ and $\Theta$ are independent), we can get rid of the density of $\Theta$ (P.432) and thus we become
	   $$\eqbox{LR(\bm y_{obs},\theta_{obs}) = \frac{f_{\bm Y|H=0,\Theta = \theta_{obs}}(\bm y_{obs})}{f_{\bm Y|H=1,\Theta = \theta_{obs}}(\bm y_{obs})}}$$
