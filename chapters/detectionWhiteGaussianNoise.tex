% !TeX root = ../zsfg-cdt.tex

\section{Detection in White Gaussian Noise}
\subsection{Setup}
We have $M\geq 2$ messages with each message having a prior $\pi_m$. We observe a continuous time SP $(Y(t))$ in the form of
$$Y(t) = s_m(t)+N(t)$$
Where $s_m$ are real deterministic signals bandlimited to $W$ hz and with $N$ being white gaussian noise.\\\\
Now we want to transform the SP to a random vector as this helps us guess.
There are the following two ways to do this.

\subsubsection{Conditional law of \texorpdfstring{$(\innerp{\bm Y}{\bm \phi_1},...,\innerp{\bm
Y}{\bm \phi_d})^{\text{T}}$}{<Y,phi1> ... <Y, phid>}}

If $(\bm \phi_1,...,\bm \phi_d)$ is an orthonormal basis of span($\bm s_1,...,\bm s_M$) then a decision rule based on $\bm Y$ is as good than a decision rule based on
$$(\innerp{\bm Y}{\bm \phi_1},...,\innerp{\bm Y}{\bm \phi_d})^{\text{T}}$$
Conditional on $M=m$ the random vector above is gaussian with mean vector
$$(\innerp{\bm s_m}{\bm\phi_1},...,\innerp{\bm s_m}{\bm \phi_d})^{\text{T}}$$
and with covariance Matrix $\frac{N_0}{2}I_d$.

\subsubsection{Conditional law of \texorpdfstring{$(\innerp{\bm Y}{\bm s_1},...,\innerp{\bm
Y}{\bm s_M})^{\text{T}}$}{<Y,s1>, ..., <Y,sM>}}


There is also no Loss in optimality by guessing based on
$$(\innerp{\bm Y}{\bm s_1},...,\innerp{\bm Y}{\bm s_M})^{\text{T}}$$
Conditional on $M=m$ the random vector above is gaussian with mean vector
$$(\innerp{\bm s_m}{\bm s_1},...,\innerp{\bm s_m}{\bm s_M})^{\text{T}}$$
and covariance Matrix
\begin{center}
  \includegraphics[width=\linewidth]{images/condconv}
\end{center}
\begin{center}
  \includegraphics[width=0.7\linewidth]{images/compinnerp}
\end{center}

\subsubsection{Conditional law of \texorpdfstring{$(\innerp{\bm Y}{\bm{\tilde{s}}_1},...,\innerp{\bm
Y}{\bm{\tilde{s}}_n})^{\text{T}}$}{<Y,~s1>, ... <Y,~sn>}}
Conditional on $M=m$, the random vector
\begin{align}
  (\innerp{\bm Y}{\bm{\tilde{s}}_1},...,\innerp{\bm Y}{\bm{\tilde{s}}_n})^{\text{T}}\\
  \intertext{is gaussian with mean vector}
  (\innerp{\bm s_m}{\bm{\tilde{s}}_1},...,\innerp{\bm s_m}{\bm{\tilde{s}}_n})^{\text{T}}
  \intertext{and $n\times n$ covariance matrix}
  \frac{N_0}{2}
  \begin{pmatrix}
    \innerp{\bm{\tilde{s}}_{1}}{\bm{\tilde{s}}_{1}} &\innerp{\bm{\tilde{s}}_{1}}{\bm{\tilde{s}}_{2}}  & \dots  &\innerp{\bm{\tilde{s}}_{1}}{\bm{\tilde{s}}_{n}} \\
    \innerp{\bm{\tilde{s}}_{2}}{\bm{\tilde{s}}_{1}} &\innerp{\bm{\tilde{s}}_{2}}{\bm{\tilde{s}}_{2}}  & \dots  &\innerp{\bm{\tilde{s}}_{2}}{\bm{\tilde{s}}_{n}} \\
    \vdots & \vdots  & \ddots & \vdots \\
    \innerp{\bm{\tilde{s}}_{n}}{\bm{\tilde{s}}_{1}} &\innerp{\bm{\tilde{s}}_{n}}{\bm{\tilde{s}}_{2}}  & \dots  &\innerp{\bm{\tilde{s}}_{n}}{\bm{\tilde{s}}_{n}}
  \end{pmatrix}.
\end{align}


\subsection{Optimal Guessing Rule}
We can thus construct either $M$ matched filters to construct a vector of $M$
values for guessing or construct $d$ matched filters for a
vector of length $d$ for guessing.

What we do here is base our guess onto the random Vector $\bm T$
$$\bm T = \big(\innerp{\bm Y}{\bm \phi_1},...,\innerp{\bm Y,\bm \phi_d}\big)^{\text{T}}$$
with
$$T^{(l)} = \innerp{\bm Y}{\bm\phi_l}=\intf Y(t)\phi_l(t)dt$$
Because we know from above the mean and variance we can form the conditional density

\begin{empheq}[box=\eqbox]{align}
  f_{ \bm T|M=m}(\bm t) &=\prod_{l=1}^d\frac{1}{\sqrt{2\pi
      N_0/2}}exp\Bigg(-\frac{\big(t^{(l)}-\innerp{\bm s_m}{\bm
  \phi_l}\big)^2}{2N_0/2}\Bigg)\\
			&= \frac{1}{(\pi
			  N_0)^{d/2}}exp\Big(-\frac{1}{N_0}\sum_{l=1}^d\big(t^{(l)}-\innerp{\bm s_m}{\bm
			\phi_l}\big)^2 \Big)
    \end{empheq}
    This is equal to the density function of section 19.8 (Multi-Dimensional M-Ary). We just need to change some stuff:
    \begin{center}
      \includegraphics[width=\linewidth]{images/section}
    \end{center}

    \subsubsection{Decision rule}
    The decision rule that guesses uniformly at random from among all the
    messages $\tilde{m} \in \mathcal M$ for which
    $$\tilde{m} = \underset{m'\in \mathcal
      M}{\text{argmax}}\left\{\ln(\pi_{m'})-\frac{\sum_{l=1}^{d}\big(\innerp{\bm Y}{\bm
    \phi_l}-\innerp{\bm s_{m'}}{\bm \phi_l}\big)^2}{N_0}\right\}$$
    minimizes the probability of error.

    \subsubsection{Decision rule if $M$ has a uniform distribution}
    The decision rule that guesses uniformly at random from among all the
    messages $\tilde{m} \in \mathcal M$ for which
    $$\tilde{m} = \underset{m'\in \mathcal M}{\text{argmin}}\Big\{\sum_{l=1}^{d}\big(\innerp{\bm Y}{\bm \phi_l}-\innerp{\bm s_{m'}}{\bm \phi_l}\big)^2\Big\}$$
    minimizes the probability of error.

    \subsubsection{Decision rule if $M$ has uniform distribution and
    \texorpdfstring{$\{\bm{\tilde{s}}_j\}$}{~sj} have same energy}
    If $||\bm s_1 ||^2 = \dots = ||\bm s_n ||^2$, then the decision rule that guesses uniformly at random from among all the
    messages $\tilde{m} \in \mathcal M$ for which
    $$\tilde{m} = \underset{m'\in \mathcal M}{\text{argmax}}\Big\{\sum_{l=1}^d\innerp{\bm Y}{\bm \phi_l}\innerp{\bm s_{m'}}{\bm\phi_l}\Big\}$$
    minimizes the probability of error.

    \subsection{Performance Analysis}
    Generally it holds that
    \begin{align}
  &p_{\text {MAP}}(\text { error } | M=m) \leq\\
  &\quad\leq\sum_{m^{\prime} \neq m} \operatorname{Pr}\left[\pi_{m^{\prime}} f_{\mathbf{Y} | M=m^{\prime}}(\mathbf{Y}) \geq \pi_{m} f_{\mathbf{Y} | M=m}(\mathbf{Y}) | M=m\right] \\
  &\quad=\sum_{m^{\prime} \neq m} \int_{\mathcal{B}_{m, m^{\prime}}} f_{\mathbf{Y} | M=m}(\mathbf{y}) \mathrm{d} \mathbf{y}
  \shortintertext{where}
  &\quad\mathcal{B}_{m, m^{\prime}}=\left\{\mathbf{y} \in \mathbb{R}^{d} : \pi_{m^{\prime}} f_{\mathbf{Y} | M=m^{\prime}}(\mathbf{y}) \geq \pi_{m} f_{\mathbf{Y} | M=m}(\mathbf{y})\right\}
    \end{align}

    \subsubsection{General Bounds}
    $$p^*(error)\leq \sum_{m\in\mathcal M} \pi_m \sum_{m'\neq
      m}Q\Bigg(\frac{||\bm s_m-\bm s_{m'}||_2}{\sqrt{2N_0}} +
      \frac{\sqrt{N_0/2}}{||\bm s_m - \bm s_{m^\prime}||_2} \text{ln}
    \frac{\pi_m}{\pi_{m^\prime}}\Bigg)$$
    $$p^*(error)\geq \sum_{m\in\mathcal M} \pi_m \underset{m'\neq m}{\text{max}}
    Q\left(\frac{||\bm s_m-\bm s_{m'}||_2}{\sqrt{2N_0}} +
      \frac{\sqrt{N_0/2}}{||\bm s_m - \bm s_{m^\prime}||} \text{ln}
    \frac{\pi_m}{\pi_{m^\prime}}\right)$$

    \subsubsection{Bounds if $M$ has a uniform distribution}
    $$p^*(error)\leq \frac{1}{M}\sum_{m\in\mathcal M}\sum_{m'\neq m}Q\Bigg(\sqrt{\frac{||\bm s_m-\bm s_{m'}||^2_2}{2N_0}}\Bigg)$$
    $$p^*(error)\geq \frac{1}{M}\sum_{m\in\mathcal M}\underset{m'\neq m}{\text{max}}Q\Bigg(\sqrt{\frac{||\bm s_m-\bm s_{m'}||^2_2}{2N_0}}\Bigg)$$
    \subsection{On Energy}
    Let the prior $\left\{\pi_{m}\right\}$ and mean signals $\left\{\mathbf{s}_{m}\right\}$ be given. Let
    $$
    \mathbf{s}_{*}=\sum_{m \in \mathcal{M}} \pi_{m} \mathbf{s}_{m}
    $$
    Then, for any energy-limited signal s
    $$
    \sum_{m \in \mathcal{M}} \pi_{m}\left\|\mathbf{s}_{m}-\mathbf{s}_{*}\right\|_{2}^{2} \leq \sum_{m \in \mathcal{M}} \pi_{m}\left\|\mathbf{s}_{m}-\mathbf{s}\right\|_{2}^{2}
    $$
    with equality if, and only if, $s$ is indistinguishable from $s_{*}$.

    This means to minimize energy transfer we have to center our signals $s^{(j)}$

