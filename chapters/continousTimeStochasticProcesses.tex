% !TeX root = ../zsfg-cdt.tex

\section{Continuous Time Stochastic Processes}
\subsection{Basics}
The process is \textbf{centered} if for every $t\in \mathbb R$ the random Variable is of zero mean.\\
The \textbf{Finite-Dimensional Distributions, FDD} of a continuous-time Stochastic Process are all the joint distributions of tuples of the form $(X(t_1),...,X(t_n))$. It has to satisfy two properties:
\begin{enumerate}
	\item it has to be symmetric, the distribution function
$F_n(\xi_{1},...,\xi_n,t_1,...,t_n) = Pr[X(t_1)\leq \xi_1,...,X(t_n) \leq \xi_n]$
does not change if we permute the $\xi_i$ and the $t_i$ by the same permutation.
\item the consistency property ? (P.561)
\end{enumerate}
Two stochastic processes are \textbf{independent} if for any epoch the $n$-tuples $(X(t_1),...,X(t_n))$ and $(Y(t_1),...,Y(t_n))$ are independent.
\subsection{Gaussian Stochastic Process}
A SP is a \textbf{gaussian stochastic Process} if for every choice of $n$ the random vector $(X(t_1),...,X(t_n))^{\text{T}}$ is gaussian.\\\\
If $(X(t))$ is a centered gaussian SP then the FDD's are determined by
$$(t_1,t_2)\mapsto Cov[X(t_1),X(t_2)]$$
(any two samples can be inserted here).

If we can write $(X(t_1), \dots, X(t_n))^T = AW$, where $W$ is a standard Gaussian,
and
\begin{equation}
	A = \begin{pmatrix} a(t_1)\\ \vdots \\ a(t_n) \end{pmatrix} \in
	\mathbb{R}^{n\times 1},
\end{equation}
then (X(t)) is a Gaussian SP.
\subsection{Stationary Continuous Time Processes}
A continuous time SP $(X(t))$ is \textbf{stationary} if for every $n\in \mathbb N$, any epoch $t_1,...,t_n \in \mathbb R$ and every $\tau\in \mathbb R$
$$(X(t_1+\tau),...,X(t_n+\tau)) \overset{\mathscr{L}}{=} (X(t_1),...,X(t_n))$$
This means that if we shift the whole process in time then the distribution
does not change! For $n = 1$ this means that all samples have the same
distribution. If $n=2$ then the joint distribution of the two depends not on the absolute value but on how far apart they have been taken.\\\\
A continuous Time SP $(X(t))$ is \textbf{wide-sense stationary, WSS} if the 3 conditions hold:
\begin{enumerate}
	\item It is of finite Variance
	\item Its mean is constant $E[X(t)] = E[X(t+\tau)]$
	\item The covariance between the samples satisfy $$Cov[X(t_1),X(t_2)] = Cov[X(t_1+\tau),X(t_2+\tau)]$$
\end{enumerate}
\textbf{Every finite Variance stationary SP is WSS.}\\\\
The \textbf{Autocovariance function} of a WSS continuous time SP is defined as
$$K_{XX}(\tau) = Cov[X(t+\tau),X(t)]$$
$$Var[X(t)]=K_{XX}(0)$$
\subsection{Stationary Gaussian Stochastic Processes}
\begin{center}
	\includegraphics[width=0.6\linewidth]{images/Gauss}
\end{center}
A Gaussian Sp is stationary iff it is WSS and the FDD's of a centered stationary Gaussian SP are fuly specified by its autocovariance function.
\subsection{Properties of the Autocovariance Function}
The autocovariance function of the process needs to be continous at the origin:
$$\underset{\delta \rightarrow 0}{\text{lim}}E[(X(t+\delta)-X(t))^2]=0$$
If the autocovariance function of a WSS continuous-time SP is continuous at the origin then it is a uniformly continuous function.\\\\
The autocovariance function $K_{XX}$ of a continuous-time WSS process $(X(t))$ is symmetric
$$K_{XX}(-\tau) = K_{XX}(\tau)$$
and it is positive semi definite
$$\sum_{\nu=1}^n\sum_{\nu'=1}^n \alpha_{\nu}\alpha_{\nu'}K_{XX}(t_{\nu}-t_{\nu'})\geq 0$$
\subsection{The Power Spectral Density}
The WSS continuous-time SP $(X(t))$ is of \textbf{Power spectral Density} $S_{XX}$ if $S_{XX}$ is nonnegative, symmetric, integrable and whose Inverse FT is $K_{XX}$
$$\eqbox{K_{XX}(\tau) = \intf S_{XX}(f)e^{i2\pi ft}df}$$
$$S_{XX}(f) \geq 0\quad S_{XX}(-f) = S_{XX}(f)$$
$$Var[X(t)] = K_{XX}(0) = \intf S_{XX}(f) df$$
\subsection{The Average Power}
The power in a centered, WSS SP $(X(t))$ of autcovariance function $K_{XX}$ is equal to $K_{XX}(0)$
\subsection{Stochastic Integrals and Linear Functionals}
We want to look at the function
$$\omega \mapsto \intf X(\omega, t)s(t) dt$$
Where $(X(t))$ is WSS and $s(t)$ is integrable. Then
the \textbf{mean} of this mapping is
$$E[X(0)]\intf s(t)dt$$
The \textbf{variance} of this mapping is
$$\intf \intf s(t)K_{XX}(t-\tau)s(\tau)d\tau dt$$$$ = \intf K_{XX}(\sigma)R_{ss}(\sigma)d\sigma$$
$$= \intf S_{XX}(f)|\hat s(f)|^2df$$
% \textbf{Notes:}
% $$\intf X(t)(\alpha_1 s_1(t)+\alpha_2s_2(t))dt =$$$$ \alpha_1\intf X(t)s_1(t)dt+\alpha_2\intf X(t)s_2(t)dt$$
\subsection{Linear Functionals of Gaussian Processes}
We assume a WSS gaussian SP $(X(t))$ and look at the Random Variable
$$Y = \intf X(t)s(t)dt+\sum^n_{\nu=1}\alpha_{\nu}X(t_{\nu})$$
then $Y$ is a Gaussian RV with mean and variance:
\begin{align}
	E[Y] =& E[X(0)]\Big(\intf s(t)dt+\sum_{\nu}^{n}\alpha_{\nu}\Big)\\
	Var[Y] =& \intf K_{XX}(\sigma)R_{ss}(\sigma)d\sigma+\\
		&+\sum_{\nu=1}^n\sum_{\nu'=1}^n\alpha_{\nu}\alpha_{\nu'}K_{XX}(t_{\nu}-t_{\nu'})+\\
		&+2\sum_{\nu}^n\alpha_{\nu}\intf s(t)K_{XX}(t-t_{\nu})dt
\end{align}
\subsection{The Joint distribution of Linear Functionals}
Now we consider m functionals with a stationary Gaussian SP $(X(t))$:
$$\intf X(t)s_j(t)dt + \sum_{\nu=1}^{n_j}\alpha_{\nu,j}X(t_{\nu,j}), \quad j=1,...,m$$
The $m$ linear functionals are \textbf{jointly Gaussian}\\\\
Assume $(X(t))$ is a WSS SP, then
\begin{align}
Cov\bigg[\intf X(t)s(t)dt,X(\tau)\bigg] &= \intf s(t)K_{XX}(\tau-t)dt\\
					  &= \intf S_{XX}(f) \hat s(f)e^{i2\pi ft}df\\
					    Cov\bigg[\intf X(t)s_1(t)dt,\intf
					    X(t)&s_2(t)dt\bigg] =\\
					  \quad&= \intf K_{XX}(\sigma)\big(s_1\star \overset{\leftarrow}{s_2}\big)(\sigma)d\sigma\\
					  &= \intf S_{XX}(f)\hat s_1(f)\hat s_2^*(f)df\\
\end{align}
For real valued $s_1,s_2$
\subsection{Filtering WSS Processes}
Filtering a stochastic process $(X(t))$  with $h(t)$ yields
$$ Y = (X(t))\star h = \intf X(\sigma)h(t-\sigma)d\sigma$$
If $(X(t))$ is centered and WSS then
\begin{enumerate}
	\item $\bm Y$ is centered, WSS with autocovariance function
		$$K_{YY} = K_{XX}\star R_{hh}\qquad S_{YY} = |\hat h(f)|^2 S_{XX}(f)$$
	\item $E\big[X(t)Y(t+\tau)\big] = \big((K_{XX}\star h\big)(\tau)$
	\item If $X$ is gaussian then so is $Y$. Moreover, the random variables $X(t_1),...,X(t_n),Y(t_1),...,Y(t_n)$ are jointly gaussian.
\end{enumerate}
\subsection{PSD after Filtering}
If $(X(t))$ is centered and WSS then the average Power of a filters output is given by
$$\text{Power of }\bm X\star\bm h = \innerp{K_{XX}}{R_{hh}}$$
$$ = \intf S_{XX}(f)|\hat h(f)|^2df$$
\subsection{White Gaussian Noise}
$(N(t))$ is white gaussian noise of double-sided power spectral Density $N_0/2$ with respect to the bandwidth W if $(N(t))$ is stationary, centered and a gaussian SP with
$$S_{NN}(f) = \frac{N_0}{2}, \qquad f\in[-W,W]$$
\begin{center}
	\includegraphics[width=\linewidth]{images/noise}
\end{center}
\textbf{Properties}\\\\
$\bm s$ integrable and bandlimited to $W$ hz:
$$\intf N(t)s(t) dt \backsim \mathcal N\Big(0,\frac{N_0}{2}||\bm s||^2_2\Big)$$
$s_1,...s_m$ integrable functions bandlimited to W hz then the m random variables
$$\intf N(t)s_1(t)dt,...,\intf N(t)s_m(t)dt$$
are jointly gaussian centered RV of covariance matrix
\begin{center}
	\includegraphics[width=\linewidth]{images/noisecov}
\end{center}
If the $s_n$ from above are orthonormal then the m RV are IID $\mathcal N(0,N_0/2)$\\\\
If s is integrable and bandlimited to W hz then $K_{NN}\star s = \frac{N_0}{2}s$ and
$$Cov\bigg[\intf N(\sigma)s(\sigma)d\sigma,N(t)\bigg] = \frac{N_0}{2}s(t)$$
\subsubsection{Projecting White Gaussian Noise}
We can project our SP that is WSS onto a orthonormal span (span($\phi_1,...,\phi_d$)). Then if $\bm N$ is white gaussian noise then
$$\sum_{l=1}^d\innerp{\bm N}{\bm{\phi}_l}\bm{\phi}_l\qquad\qquad \text{projection on span($\phi_1,...,\phi_d)$}$$
$$\bm N - \sum_{l=1}^d\innerp{\bm N}{\bm{\phi}_l}\bm{\phi}_l \qquad\text{orthogonal to span($\phi_1,...,\phi_d)$}$$
Now it we add the two parts we get $N$ back. The magic now comes from the fact that if we keep the projection the same, but construct ourself some $\bm N'$ that has exactly the same law but is independent of $\bm N$ then if we add the two we get $N$ back:
$$ \sum_{l=1}^d\innerp{\bm N}{\bm{\phi}_l}\bm{\phi}_l + \bm N' - \sum_{l=1}^d\innerp{\bm N'}{\bm{\phi}_l}\bm{\phi}_l$$
Has the same law than $\bm N$
