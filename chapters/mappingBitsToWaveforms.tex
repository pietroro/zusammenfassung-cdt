% !TeX root = ../zsfg-cdt.tex

\section{Mapping Bits to Waveforms}
Modulation is the act of mapping bits to waveforms.\\\\
What we want is some mapping $\phi(\cdot)$ that maps length $k$ sequences of
bits to length $n$ sequences of real numbers:
$$\bm \varphi: \{0,1\}^k \rightarrow \mathbb{R}^n$$
$$(d_1,...,d_k) \mapsto (x_1,...,x_n)$$
A two level signal (where $X_j$ can be two distinct values) has $n = k$. This mapping would have a rate of one bit per real symbol.\\
A four level signal (4 distinct values) has $n=k/2$ (two bits per real symbol).
\subsection{Block Mode Mapping}
We take from the $k$ Databits a $K$-tuple (meaning we take $K$ bits and put
them together). This $K$-tuple is then encoded to an $N$-Tuple consisting of
$N$ real numbers. This is called a \textbf{(K,N) block encoder}. The rate is defined as
$$\frac{K}{N}\quad bits/real symbol$$
\begin{itemize}
  \item $k$ needs to be divisible by $K$
  \item $n$ is given by $\frac{k}{K}N$
  \item The $K$-tuples are encoded individually as seen in the next figure
\end{itemize}
\begin{center}
  \includegraphics[width=\linewidth]{images/blockmode}
\end{center}
If $k$ is not divisible by $K$ then we just insert zero's until it is ($k' = \ceil[\Big]{\frac{k}{K}}\cdot K\qquad n' = \frac{k'}{K}N$)
\subsection{Linear Modulation}
In a linear modulation scheme the transmitted Waveform $\bm X$ is given as
$$X(t) = A\sum_{l = 1}^{n}X_lg_l(t)$$
Where $X_l$ are the mapped real numbers (the bits D have already been mapped to X).\\
\textbf{The transmitted Energy} is given by
\begin{align}
  ||\bm X||^2_2 &= \intf X^2(t)dt\\
& = A^2\sum_{l=1}^m\sum_{l'=1}^n X_lX_{l'}\intf g_l(t)g_{l'}(t)dt
\intertext{If the waveforms are orthonormal then the inner product between them
is zero if $(l\neq l')$}
||\bm X||^2_2 &= A^2\sum_{l=1}^nX_l^2
\end{align}
\subsubsection{Recovering the Coefficients}
Assume a mapping  $X(t) = A\sum_{l=1}^nX_l\phi_l(t)$ where the $\phi$'s are orthonormal. In this case it is easy to recover $X_l$ because
$$X_l = \frac{1}{A}\innerp{\bm X}{\bm{\phi}_l}$$
If furthermore the $\phi_l$ have the form
$$\phi_l = \phi(t-lT_s)$$ then it simplifies even more as we can use one matched filter and just sample it at the appropriate times.
$$X_l = \frac{1}{A}(X\star\overset{\leftarrow}{\phi})(lT_s)$$
(in the complex case it is $\overset{\leftarrow}{\phi^*}$)
\begin{center}
  \includegraphics[width=\linewidth]{images/matched}
\end{center}
\subsection{Pulse Amplitude Modulation (PAM)}
PAM is given by the waveform
$$\eqbox{X(t) = A\sum_{l=1}^{n}X_lg(t-lT_s)}$$
where $\bm g$ is real and called the \textbf{Pulse shape}, $T_s$ is called the \textbf{baud Period} and $1/T_s$ is called the \textbf{baud rate} which gives the real symbols per second and $A>0$.\\\\
The \textbf{Constellation}~$\chi$ is just what the bits can be mapped to, for example for 4 level the constellation is $\{-3,-1,1,-3\}$. The \textbf{number of points} would be 4 for this constellation. The \textbf{Minimum distance} is the euclidean distance between the two closest points in the constellation (here its 2).
\subsection{Considerations}
\textbf{{Bandwidth:}}\\
The bandwidth of the sent waveform is equal to the
bandwidth of the pulseshape $\bm g$.\\\\
\textbf{Design:}\\
\textbf{Data Rate} = $R_b = \frac{1}{T_s}\frac{K}{N}$  bits/second\\\\
\textbf{Power} is dependent of A, $T_s$ and the encoder.\\\\
\textbf{Power Spectral density} is affected by A, $T_s$ and $\phi$\\\\
\textbf{Bandwidth} is determined by $\phi$ and must satisfy $W\geq \frac{1}{2T_s}$ for orthonormality.\\\\
\textbf{Probability of error} is affected by A, $T_s$ and the encoder.
